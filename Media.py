#  Media.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


import os

from lxml import etree as et

from OpenXML.core import *
from OpenXML.Namespaces import *


# TODO: Document _everything_


class Image:
    def __init__(self, name, docx, node, description, pic_id, drawing_id, rel_id, dpi=300):
        if not os.path.exists(name):
            raise OpenXMLException('Cannot find file ' + name + '.')

        self.name = name
        self.docx = docx
        self.node = node
        self.descr = description

        self._ids = {
            'picture': pic_id,
            'drawing': drawing_id,
            'relationship': rel_id,
            }
        # In pixels
        x, y = png_dimensions(name)
        # In EMUs
        self.x = str(int(x * 914400 / dpi))
        self.y = str(int(y * 914400 / dpi))


    @property
    def base_name(self):
        return os.path.basename(self.name)


    @property
    def arc_path(self):
        return 'media/' + self.base_name


    def id(self, type_):
        if type_ in self._ids:
            return str(self._ids[type_])
        raise OpenXMLException('Image does not have id of type ' + str(type_) + '.')


    # TODO: Remove explicit reference to word/document.xml
    def inside_run(self):
        parent = self.node.getparent()
        root = self.docx.etree('word/document.xml')
        while parent != root:
            if et.QName(parent).localname == 'r':
                return True
            parent = parent.getparent()
        return False


    def xml(self):
        drawing = W.drawing(
            WP.inline(# Attrs at end!
                WP.extent(cx=self.x, cy=self.y),
                WP.effectExtent(l='0', t='0', r='0', b='0'),
                WP.docPr(id=self.id('drawing'), name=self.base_name,
                         descr=self.descr),
                WP.cNvGraphicFramePr(
                    A.graphicFrameLocks(noChangeAspect='1')
                    ),
                A.graphic(A.graphicData(# Attrs at end!
                        PIC.pic(
                            PIC.nvPicPr(
                                PIC.cNvPr(id=self.id('picture'), name=self.base_name),
                                PIC.cNvPicPr()
                                ),
                            PIC.blipFill(
                                A.blip(# Attrs at end!
                                    A.extLst(
                                        A.ext(# Attrs at end!
                                            A14.useLocalDpi(val='0'),
                                            uri=EXT_URI)
                                        )
#                                   r:embed goes here...added below.
                                    ), # / A.blip
                                A.stretch(A.fillRect())
                                ), # / PIC.blipFill
                            PIC.spPr(
                                A.xfrm(
                                    A.off(x='0', y='0'),
                                    A.ext(cx=self.x, cy=self.y)
                                    ),
                                A.prstGeom(# Attrs at end!
                                    A.avLst(),
                                    prst='rect'
                                    ),
                                ), # / PIC.spPr
                            ), # / PIC.pic
                        uri='http://schemas.openxmlformats.org/drawingml/2006/picture')
                          ), # / A.graphic
                distL='0', distR='0', distT='0', distB='0')) # / WP.inline / W.drawing

        # TODO: Make sure we _can_ be inside a run. This may be
        # unnecessary.
        if not self.inside_run():
            drawing = W.r(W.rPr(W.noProof()), drawing)

        # Word decides to throw in a random namespaced attribute. It
        # can't be dealt with above, so we'll just add it here.
        blip = drawing.find('.//a:blip', namespaces=nsmap)
        blip.attrib[N('r', 'embed')] = self.id('relationship')

        return stringify(drawing, xml_decl=False)
