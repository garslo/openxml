# OpenXML #

OpenXML for Python aims to give Python users a simple interface to
OpenXML documents. It is currently coupled with and a slave to
[ennui][ennui], however the long-term goal is to develop the package
on it's own.

OpenXML for Python is *very* alpha; don't use on anything important.

# Documentation and Help #

See the [Wiki][wiki] for more details and documentation.

# Functionality #

Given a Microsoft Word document "MyDoc.docx", we can do a few things:

    :::python
    >>> from OpenXML import *
    >>> doc = Docx.Docx('MyDoc.docx')
    >>> # Fetch each oMath element
    >>> maths = doc.etree('word/document.xml').findall('.//m:oMath', namespaces=Namespaces.nsmap)
    >>> for math in maths:
    ....    print(MathML.latex.to_latex(math))
    first LaTeX rendering
    second LaTeX rendering
    ...
    last LaTeX rendering
    >>> # Replace a node with an image
    >>> to_replace = maths[0]
    >>> doc.media_manager.add_image('myimage.png', to_replace, 'my image description')


[ennui]: http://bitbucket.org/garslo/ennui
[wiki]: http://bitbucket.org/garslo/openxml/wiki
