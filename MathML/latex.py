# -*- coding: utf-8 -*-

#  latex.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


from OpenXML.core import *
from OpenXML.MathML.core import *


def to_latex(root):
    '''
    Returns a string containing a LaTeX representation of all the
    Office MathML found in `root'.
    '''
    return translate(root, _handlers)


# Note: The values here are only valid _just_ after you've called
# to_latex(), and even then only sort of. It's better than nothing
# though. More specifically, it uses the last rPr/rFonts attributes
# we've encountered; if that's ok with you, use it.
run_properties = {}


# Uppercase:
# (loop for i in
#       '(91 92 93 94 95 96 97 98 99 9a 9b 9c 9d 9e 9f a0 a1 a2 a3 a4 a5 a6 a7 a8 a9)
#       do (ucs-insert (format "03%s" i)))
#
# Lowercase:
# (loop for i in
#       '(b1 b2 b3 b4 b5 b6 b7 b8 b9 ba bb bc bd be bf c0 c1 c2 c3 c4 c5 c6 c7 c8 c9)
#       do (ucs-insert (format "03%s" i)))
_letter_dict = {
    'Α': r'\Alpha',
    'Β': r'\Beta',
    'Γ': r'\Gamma',
    'Δ': r'\Delta',
    'Ε': r'\Epsilon',
    'Ζ': r'\Zeta',
    'Η': r'\Eta',
    'Θ': r'\Theta',
    'Ι': r'\Iota',
    'Κ': r'\Kappa',
    'Λ': r'\Lambda',
    'Μ': r'\Mu',
    'Ν': r'\Nu',
    'Ξ': r'\Xi',
    'Ο': r'\Omicron',
    'Π': r'\Pi',
    'Ρ': r'\Rho',
    'Σ': r'\Sigma',
    'Τ': r'\Tau',
    'Υ': r'\Upsilon',
    'Φ': r'\Phi',
    'Χ': r'\Chi',
    'Ψ': r'\Psi',
    'Ω': r'\Omega',
    'α': r'\alpha',
    'β': r'\beta',
    'γ': r'\gamma',
    'δ': r'\delta',
    'ε': r'\epsilon',
    'ζ': r'\zeta',
    'η': r'\eta',
    'θ': r'\theta',
    'ι': r'\iota',
    'κ': r'\kappa',
    'λ': r'\lambda',
    'μ': r'\mu',
    'ν': r'\nu',
    'ξ': r'\xi',
    'ο': r'\omicron',
    'π': r'\pi',
    'ρ': r'\rho',
    'σ': r'\sigma',
    'τ': r'\tau',
    'υ': r'\upsilon',
    'φ': r'\phi',
    'χ': r'\chi',
    'ψ': r'\psi',
    'ω': r'\omega',
    '⁡':  r'', # Function application char: U+2061 (no visual)
    '≤': r'\le',
    '≥': r'\ge',
    '≈': r'\approx',
    }


def _barH(root):
    e = mget(root, 'e')
    return wrap(r'\bar{', rest(e), '}')


def _borderBoxH(root):
    e = mget(root, 'e')
    return wrap(r'\box{', rest(e), '}')


def _boxH(root):
    # A box is just an internal grouping mechanism.
    return rest(mget(root, 'e'))


def _delimiterH(root):
    e = mget(root, 'e')
    # m:d defaults to parentheses
    ldelim = '('
    rdelim = ')'
    lxpath = xpath(root, '/m:d/m:dPr/m:begChr/@m:val')
    rxpath = xpath(root, '/m:d/m:dPr/m:endChr/@m:val')
    if len(lxpath):
        ldelim = lxpath[0]
    if len(rxpath):
        rdelim = rxpath[0]
    return wrap(r'\left'+ldelim, rest(e), r'\right'+rdelim)


# TODO: Need example document
def _eqArrH(root):
    return rest(root)


def _fractionH(root):
    num = mget(root, 'num')
    den = mget(root, 'den')
    return r'\frac{{{num}}}{{{den}}}'.format(num=rest(num),
                                             den=rest(den))


def _funcH(root):
    fName = mget(root, 'fName')
    e = mget(root, 'e')
    return r'\{fName} {args}'.format(fName=rest(fName), args=rest(e))


# TODO: The LaTeX output is NOT a lower limit!
def _limLowH(root):
    e = mget(root, 'e')
    lim = mget(root, 'lim')
    return r'{e}_{{{lim}}}'.format(e=rest(e), lim=rest(lim))


# TODO: The LaTeX output is NOT an upper limit!
def _limUppH(root):
    e = mget(root, 'e')
    lim = mget(root, 'lim')
    return r'{e}^{{{lim}}}'.format(e=rest(e), lim=rest(lim))


def _naryH(root):
#    op = ''
#    try:
#        op = nary_operator(root)
#    except MathMLReaderException:
#        # The operator doesn't correspond to anything we know, let's
#        # be kind and skip things.
#        pass
#    sub = xpath(root, 'm:sub')
#    sup = xpath(root, 'm:sup')
    return rest(root)


def _runH(root):
    # Update the run_properties
    rPr = xpath(root, 'w:rPr')
    if len(rPr):
        _rPr(rPr[0])
    # We'll allow for empty text runs
    text = xpath(root, 'm:t')
    if not len(text):
        return ''        # We just have an empty run.
    return ' '.join([replace_all(elem.text, _letter_dict) for elem in text])


# Called by _runH
def _rPr(root):
    global run_properties
    for child in list(root):
        for attr, value in child.attrib.items():
            run_properties[attr.split('}')[-1]] = value


# TODO: Test this
def _radH(root):
    e = mget(root, 'e')
    deg_xpath = xpath(root, 'm:deg')
    if len(deg_xpath):
        deg = xpath(deg_xpath[0], 'm:r/m:t')
        if len(deg):
            return r'\sqrt[deg]{{{expr}}}'.format(expr=rest(e), deg=deg[0].text)
    return wrap(r'\sqrt{', rest(e), '}')


# TODO
def _sPreH(root):
    sub = ''
    sup = ''
    return r'{{}}_{{{sub}}}^{{{sup}}}{expr}'.format(sub=sub, sup=sup,
                                                        expr=rest(root))


def _sSubH(root):
    sub = ''
    return r'{expr}_{{{sub}}}'.format(sub=sub)


def _sSubSupH(root):
    sub = ''
    sup = ''
    return r'{expr}_{{{sub}}}^{{{sup}}}'.format(expr=rest(root),
                                                    sub=sub, sup=sup)


def _sSupH(root):
    e = mget(root, 'e')
    sup = mget(root, 'sup')
    return r'{expr}^{{{sup}}}'.format(expr=rest(e), sup=rest(sup))


# These are most of the "top-level" elements that can exist directly
# under an oMath or oMathPara. There are more, (like m == matrix), but
# we're not messing with that for now.
_handlers = {
    'bar'       : _barH,
    'borderBox' : _borderBoxH,
    'box'       : _boxH,
    'd'         : _delimiterH,
    'eqArr'     : _eqArrH,
    'f'         : _fractionH,
    'func'      : _funcH,
    'limLow'    : _limLowH,
    'limUpp'    : _limUppH,
    'nary'      : _naryH,
    'r'         : _runH,
    'rPr'       : _rPr,         # This is usually only called by _runH
    'rad'       : _radH,
    'sPre'      : _sPreH,
    'sSub'      : _sSubH,
    'sSubSup'   : _sSubSupH,
    'sSup'      : _sSupH,
    }
