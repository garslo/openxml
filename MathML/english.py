# -*- coding: utf-8 -*-

#  english.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.



import re

from OpenXML.core import *
from OpenXML.MathML.core import *

def to_english(root):
    raw_english = translate(root, _handlers)
    # Rather than nitpick each little function for whitespace, we'll
    # just replace any annoying runs with a single space here.
    return re.sub(r'\s+', ' ', raw_english)


_letter_dict = {
    'Α': r'Alpha',
    'Β': r'Beta',
    'Γ': r'Gamma',
    'Δ': r'Delta',
    'Ε': r'Epsilon',
    'Ζ': r'Zeta',
    'Η': r'Eta',
    'Θ': r'Theta',
    'Ι': r'Iota',
    'Κ': r'Kappa',
    'Λ': r'Lambda',
    'Μ': r'Mu',
    'Ν': r'Nu',
    'Ξ': r'Xi',
    'Ο': r'Omicron',
    'Π': r'Pi',
    'Ρ': r'Rho',
    'Σ': r'Sigma',
    'Τ': r'Tau',
    'Υ': r'Upsilon',
    'Φ': r'Phi',
    'Χ': r'Chi',
    'Ψ': r'Psi',
    'Ω': r'Omega',
    'α': r'alpha',
    'β': r'beta',
    'γ': r'gamma',
    'δ': r'delta',
    'ε': r'epsilon',
    'ζ': r'zeta',
    'η': r'eta',
    'θ': r'theta',
    'ι': r'iota',
    'κ': r'kappa',
    'λ': r'lambda',
    'μ': r'mu',
    'ν': r'nu',
    'ξ': r'xi',
    'ο': r'omicron',
    'π': r'pi',
    'ρ': r'rho',
    'σ': r'sigma',
    'τ': r'tau',
    'υ': r'upsilon',
    'φ': r'phi',
    'χ': r'chi',
    'ψ': r'psi',
    'ω': r'omega',
    '-': 'minus',
    '+': 'plus',
    '=': 'equals',
    '≈': 'approximately equals',
    '(': 'left parenthesis',
    ')': 'right parenthesis',
    '[': 'left square bracket',
    ']': 'right square bracket',
    '{': 'left curly bracket',
    '}': 'right curly bracket',
    '≤': 'less than or equal to',
    '≥': 'greater than or equal to',
    '<': 'less than',
    '>': 'greater than',
    '∞': 'infinity',
    '°': 'degrees',
    '⁡':  r'', # Function application char: U+2061 (no visual)
    # Don't work: replace_all does a char-by-char search. Perhaps wrap
    # this replacement in _funcH?
#    'sin': 'sine',
#    'cos': 'cosine',
#    'tan': 'tangent',
#    'csc': 'cosecant',
#    'sec': 'secant',
#    'cot': 'cotangent',
    'I': 'eye',
    'a': 'eh',
    }


def _barH(root):
    # This could be tricky...for now we assume only variables have
    # bars.
    e = mget(root, 'e')
    return '{expr} bar'.format(expr=rest(e))


def _borderBoxH(root):
    e = mget(root, 'e')
    return 'start box {expr} end box'.format(expr=rest(e))


def _boxH(root):
    return rest(mget(root, 'e'))


# TODO: How does JAWS, NVDA handle '[]', '()', '{}', etc.?
def _delimiterH(root):
    e = mget(root, 'e')
    ldelim = '('
    rdelim = ')'
    lxpath = xpath(root, '/m:d/m:dPr/m:begChr/@m:val')
    rxpath = xpath(root, '/m:d/m:dPr/m:endChr/@m:val')
    if len(lxpath):
        ldelim = lxpath[0]
    if len(rxpath):
        rdelim = rxpath[0]
    return wrap(ldelim, rest(e), rdelim)


def _eqArrH(root):
    return rest(root)


def _fractionH(root):
    num = rest(mget(root, 'num'))
    den = rest(mget(root, 'den'))
    # This is arbitrary...
    min_len = 4
    if len(num.strip()) <= min_len and len(den.strip()) <= min_len:
        return 'fraction {num} over {den}'.format(num=num, den=den)
    return 'fraction with numerator {num} and denominator {den}'.format(num=num, den=den)


# TODO: double-check this (args)
def _funcH(root):
    fName = mget(root, 'fName')
    e = mget(root, 'e')
    nargs = len(e)
    if nargs is 1:
        return 'function {fName} with argument {arg}'.format(fName=rest(fName),
                                                             arg=rest(e))
    args = []
    i = 1
    for arg in e:
        args.append('argument' + str(i) + ' is ')
        args.append(rest(arg))
        i += 1
    return 'function {fName} with {nargs} arguments: {args}'.format(fName=fName,
                                                                    nags=nargs,
                                                                    args=','.join(args))


def _limLowH(root):
    e = mget(root, 'e')
    lim = mget(root, 'lim')
    return 'group {expr} with lower limit {lim}'.format(expr=rest(e),
                                                        lim=rest(lim))


def _limUppH(root):
    e = mget(root, 'e')
    lim = mget(root, 'lim')
    return 'group {expr} with upper limit {lim}'.format(expr=rest(e),
                                                        lim=rest(lim))


def _naryH(root):
    return rest(root)


# TODO: There's some messiness with function application: if f(x) is
# the application, the whole text run is (x), but we don't want
# "function f of left parenthesis x right parenthesis".
def _runH(root):
    text_elems = xpath(root, 'm:t')
    if not len(text_elems):
        return ''
    # We want to filter out all the empty text elements (it causes
    # "convert" to complain). First remove any excess whitespace:
    stripped_text = [txt.text.strip() for txt in text_elems]
    # Then filter out any resulting empty strings:
    text = [t for t in stripped_text if t is not '']
    # Finally, replace greek chars (e.g. α, β) with textual
    # equivalents (alpha, beta).
    return ' '.join([replace_all(t, _letter_dict) for t in text])


def _radH(root):
    e = mget(root, 'e')
    deg_xpath = xpath(root, 'm:deg')
    if len(deg_xpath):
        deg = xpath(deg_xpath[0], 'm:r/m:t')
        if len(deg):
            return 'the {deg} root of {expr}'.format(deg=cardinal(deg[0].text),
                                                     expr=rest(e))
    return 'the square root of {expr}'.format(expr=rest(e))


def _sPreH(root):
    return rest(root)


def _sSubH(root):
    return rest(root)


def _sSubSupH(root):
    return rest(root)


def _sSupH(root):
    return rest(root)


_handlers = {
    'bar'       : _barH,
    'borderBox' : _borderBoxH,
    'box'       : _boxH,
    'd'         : _delimiterH,
    'eqArr'     : _eqArrH,
    'f'         : _fractionH,
    'func'      : _funcH,
    'limLow'    : _limLowH,
    'limUpp'    : _limUppH,
    'nary'      : _naryH,
    'r'         : _runH,
    'rad'       : _radH,
    'sPre'      : _sPreH,
    'sSub'      : _sSubH,
    'sSubSup'   : _sSubSupH,
    'sSup'      : _sSupH,
    }
