# -*- coding: utf-8 -*-

#  core.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


from OpenXML.core import *

class MathMLReaderException(OpenXMLException):
    '''Exception class for MathML parsing.'''
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg


handlers = {}


# Maps unicode code points to their (non-standard) names.
nary_operators = {
    '222b': 'integral',         # ∫
    '222c': 'double integral',  # ∬
    '222a': 'union',            # ∪
    '2229': 'intersection',     # ∩
    '2228': 'logical or',       # ∨
    '2227': 'logical and',      # ∧
    }


# TODO: documentation!
def dispatch(root):
    if isinstance(root, list):
        return ' '.join([dispatch(elem) for elem in root])
    tag = no_ns(root.tag)
    if tag in handlers:
        return handlers[tag](root)
    return ''


def mget(root, tag):
    return get(root, tag, ns='m')


def rest(root):
    '''
    Convenience function.
    '''
    return dispatch(children(root))


def nary_operator(root):
    '''
    Returns the name (as defined in the nary_operators dict)
    corresponding to the nary operator represented by `root'. This is
    most useful when writing the handler for the 'nary' MathML tag.

    MathML represents the operator as a decimal-valued HTML
    entity. nary_operator does the proper conversions.

    Example:
    # root is an nary element with <m:chr m:val="&#8747;"/>
    >>> nary_operator(root)
    'integral'
    '''
    op_x = xpath(root, 'm:naryPr/m:chr/@m:val')
    check(op_x, build_missing_err('val', 'nary/m:naryPr/m:chr'))
    op_code_point = as_code_point(op_x[0])
    if op_code_point not in nary_operators:
        raise MathMLReaderException('Cannot determine operator type (code point {cp}).'.format(cp=op_code_point))
    return nary_operators[op_code_point]


def as_code_point(html_entity):
    '''
    Returns a string representation of the (hex) unicode code point
    corresponding to the decimal HTML entity form `html_entity'.

    Example:
    >>> as_code_point('&#8746;')
    '222a'
    '''
    return hex(int(html_entity[2:-1]))[2:]


def replace_all(text, letter_dict):
    '''
    Returns `text' with each character replaced by its corresponding
    value in `letter_dict' (if available). This is most useful when
    parsing text runs.

    See MathML.latex or MathML.english for an example of how `letter_dict'
    should be formatted.

    Example:
    >>> letter_dict = {'Φ': r'\Phi', 'π': r'\pi'}
    >>> replace_all('Φ(x) = π')
    r'\Phi (x) = \pi '
    '''
    chars = []
    for ch in text:
        if ch in letter_dict:
            # We need to make sure the new "symbol" is distinct from
            # its surroundings. Hence, we tack on a space before and
            # after E.g. aαb -> 'aalphab'' is different from aαb ->
            # 'a alpha b'
            chars.append(' ')
            chars.append(letter_dict[ch])
            chars.append(' ')
        else:
            chars.append(ch)
    return ''.join(chars)


# TODO: Make sure the example works :)
def translate(root, handler_dict):
    '''
    Iterates through `root' and, upon finding a child element whose
    tag is in `handler_dict', runs the appropriate function, passing
    the child to the function.

    See MathML.latex and MathML.english for real examples of this.

    Example:
    >>> def fooH(root): return 'processed a foo '
    ...
    >>> def barH(root): return 'processed a bar '
    ...
    >>> handler_dict = {'foo': fooH, 'bar': barH}
    ...
    >>> xml = etree.XML('<foo><bar><bar></bar></bar></foo>')
    ...
    >>> translate(xml, handler_dict)
    'processed a foo processed a bar processed a bar'
    '''
    set_handler_map(handler_dict)
    if isinstance(root, Element):
        return dispatch(root.root)
    return dispatch(root)


def cardinal(num):
    '''
    Returns a string representing the cardinal form of `num'. The
    parameter `num' can be an integer or a string containing an
    integer.

    Example:
    >>> cardinal('1')
    '1st'
    >>> cardinal('12')
    '12th'
    >>> cardinal(442)
    '442nd'
    >>> cardinal('not an int')
    Traceback (most recent last call)
    ...
    MathMLReaderException: Cannot find cardinal form of "not an int".
    '''
    if isinstance(num, int):
        return cardinal(str(num))
    # 11, 12, and 13 don't fit the normal pattern
    if num[-2:] in ['11', '12', '13']:
        return '{n}th'.format(n=num)
    if num[-1] == '1':
        return '{n}st'.format(n=num)
    if num[-1] == '2':
        return '{n}nd'.format(n=num)
    if num[-1] == '3':
        return '{n}rd'.format(n=num)
    if num[-1] in ['4', '5', '6', '7', '8', '9', '0']:
        return '{n}th'.format(n=num)
    # It's not really an integer
    raise MathMLReaderException('Cannot find cardinal form of "' + str(num) + '".')

def set_handler_map(handler_map):
    '''
    Sets the global handler map that MathML.core.dispatch uses.
    '''
    global handlers
    handlers = handler_map


def build_element(root):
    '''
    Returns an Element representing `root'. `root' can be either an
    etree or a list.
    '''
    if isinstance(root, list):
        if not len(root):
            raise MathMLReaderException('Cannot build an element with an empty root')
        return build_element(root[0])
    return Element(root, no_ns(root.tag))


# TODO: documentation!
class Element:
    def __init__(self, root, e_type):
        self._root = root
        self._e_type = e_type

    @property
    def root(self):
        return self._root

    @property
    def e_type(self):
        return self._e_type

    def __getitem__(self, key):
        '''The properties'''
        return build_element(get(self.root, key, ns='m'))
