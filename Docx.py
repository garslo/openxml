#  Docx.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


import os
import re
import shutil
import tempfile
import zipfile

from lxml import etree as et

from OpenXML.Namespaces import *
from OpenXML.Managers import *
from OpenXML.core import *

import tools


def _is_safe(docx_file):
    '''
    Checks if any files in `zipfile' are absolute paths or contain '..'
    prefixes. If so, returns False. Otherwise, returns True.
    '''
    for path in zipfile.ZipFile(docx_file).namelist():
        if os.path.isabs(path) or path[:2] == '..':
            return False
    return True


class DocxException(OpenXMLException):
    pass


class Docx:
    def __init__(self, filename):
        self.name = filename
        self._files_loaned = {} # See self.etree() comments
        self.media_manager = MediaManager(self)
        self.relationship_manager = RelationshipManager(self)
        self.content_type_manager = ContentTypeManager(self)


    @property
    def base_name(self):
        os.path.basename(self.name)


    @property
    def new_zip(self):
        if self.name.find('.') != -1:
            return self.name.split('.')[-2] + '-ADA.docx'
        return self.name + '-ADA.docx'


    # TODO: Document this further. Mainly comments explaining the
    # self._files_loaned stuff.
    def etree(self, fname):
        '''
        Returns an lxml.etree corresponding to `fname' as found in the
        zip archive.
        '''
        # We're keeping track of each file we've given out. The
        # changes anyone makes on a particular tree are not
        # immediately written to the docx we're reading from, so we
        # make sure everyone's working on the same etree.
        if fname not in self._files_loaned:
            self._files_loaned[fname] = et.XML(self.retrieve(fname))
        return self._files_loaned[fname]


    def retrieve(self, fname):
        '''
        Returns the contents of `fname' as a string. If `fname'
        is not part of the zip archive, throws a DocxException.

        Example:
        >>> contents = Docx('myfile.docx').retrieve('word/document.xml')
        >>> contents
        <Contents of word/document.xml>
        '''
        try:
            with zipfile.ZipFile(self.name).open(fname) as fh:
                return fh.read()
        except KeyError:
            raise DocxException('The archive does not contain ' + str(fname))


    def commit(self):
        '''
        Commits any changes to the archive, saving it along-side the
        original docx file. Does not overwrite the original file.
        '''
        # We don't want to extract anything with absolute paths or '..' prefixes.
        if not _is_safe(self.name):
            raise DocxException('Will not extract {filename}; it contains unsafe paths.'.format(filename=self.name))

        # We'll just dump everything into a temp folder, overwrite anything
        # that's been self.add()'ed, and create a new archive.
        temp = tempfile.mkdtemp()
        with zipfile.ZipFile(self.name) as z:
            z.extractall(temp)

        # Dump all the loaned stuff
        for filename, etree in self._files_loaned.items():
            write_path = os.path.join(temp, filename)
            xml = et.tostring(etree, encoding="UTF-8", xml_declaration=True,
                              standalone=True)
            with open(write_path, 'w') as fh:
                fh.write(xml.decode('utf-8'))

        # Add the media
        media_path = os.path.join(temp, 'word', 'media')
        if not os.path.exists(media_path):
            os.mkdir(media_path)
        self.media_manager.write(temp)

        # Create a new docx.
        with zipfile.ZipFile(self.new_zip, 'w') as z:
            for root, dirs, files in os.walk(temp):
                for file_ in files:
                    rel_path = os.path.relpath(root, temp)
                    real_path = os.path.join(root, file_)
                    arc_name = os.path.join(rel_path, file_)
                    z.write(real_path, arcname=arc_name)

        # And clean up.
        shutil.rmtree(temp)
