#  core.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.

from lxml import etree as et


class OpenXMLException(Exception):
    '''Exception class for OpenXML.'''
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg


def check(root, err_msg):
    '''
    Convenience function which checks if `root' is actually an
    element. If not, raises an OpenXMLException with message
    `err_msg'.
    '''
    if not len(root):
        raise OpenXMLException(err_msg)


def no_ns(tag):
    '''
    Returns a string representing `tag' stripped of any lxml-style
    namespace prefixes.

    Example:
    >>> tag = '{http://my.very.long/namespace/declaration}p'
    >>> no_ns(tag)
    'p'
    '''
    return tag.split('}')[-1]


def xpath(root, path):
    '''
    Convenience function.
    '''
    return root.xpath(path, namespaces=root.nsmap)


def children(root):
    '''
    Returns the child elements of `root'. `root' may be an lxml
    element or a list of lxml elements.

    Example:
    >>> xml = etree.XML('<foo><bar></bar><bar></bar></foo>')
    >>> children(xml)
    [<Element bar at 0xdeadbeef>, <Element bar at 0xdeadcode>]
    '''
    if isinstance(root, list):
        return [children(elem) for elem in root]
    return list(root)


def wrap(start, text, end):
    '''
    Wraps `text' between `start' and `end'. This is done so often it's
    helpful to abstract it and, eventually, find the most efficient
    way to wrap text.

    Example:
    >>> wrap(r'\bar{', 'x', '}')
    r'\bar{x}'
    '''
    return r'{start}{text}{end}'.format(start=start,
                                        text=text,
                                        end=end)


# TODO: Change default ns to None, handle accordingly.
def build_missing_err(tag, missing, ns='m'):
    '''
    Convenience function.

    Example:
    >>> build_missing_err('mytag', 'notfound')
    'Cannot find m:notfound in m:mytag'
    '''
    return 'Cannot find {ns}:{missing} in {ns}:{tag}'.format(ns=ns,
                                                             missing=missing,
                                                             tag=tag)


def get(root, tag, ns=None):
    '''
    Convenience function that does an xpath search on `root' for
    `tag'. If there are no results found, raises an
    OpenXMLException.

    Example:
    >>> from lxml import etree as et
    >>> xml_text = """<m:oMath xmlns:m="the/namespace/for/office/math">
      <m:f>
        <m:num>
          <m:r><m:t>4</m:t></m:r>
        </m:num>
        <m:den>
          <m:r><m:t>5</m:t></m:r>
        </m:den>
      </m:f>
    </m:oMath>"""
    >>> xml = et.XML(xml_text)
    >>> get(xml, 'f', ns='m')
    [<Element {the/namespace/for/office/math}f at 0xdeadbeef>]
    >>> get(xml, 'num', ns='m')
    Traceback (most recent call last)
    ...
    OpenXMLException: Cannot find m:num in m:oMath
    '''
    if ns:
        tag = '{ns}:{tag}'.format(ns=ns, tag=tag)
    elem = xpath(root, tag)
    check(elem, build_missing_err(root.tag, tag, ns=ns))
    return elem


def stringify(node, xml_decl=True):
    '''
    Returns `node` as a string, optionally prefixed with an xml
    declaration as specified by `xml_decl`.
    '''
    return et.tostring(node, encoding='utf-8', xml_declaration=xml_decl)


def png_dimensions(filename):
    '''
    Returns a tuple consisting of the width and height of the PNG
    image (in pixels) named filename.

    Example:
    # image.png contains a PNG of dimension 50x100 pixels
    >>> width, height = png_dimensions('image.png')
    >>> width
    50
    >>> height
    100
    >>> png_dimensions('not_a_png.jpg')
    Traceback (most recent call last)
    ...
    Exception: File not_a_png.jpg is not a png image.
    '''
    with open(filename,  'rb') as fh:
        # A PNG header stores the width from bytes [16, 20) and the
        # height from bytes [20, 24). The PNG file signature is in the
        # first 8 bytes; it's hex representation is 89 50 4e 47 0d 0a 1a 0a.
        sig = fh.read(8)   # First we check the file signature
        if sig != bytes.fromhex('89504e470d0a1a0a'):
            raise OpenXMLException('File {f} is not a png image.'.format(f=filename))
        fh.read(8)   # Skip to the 16th byte
        width = int.from_bytes(fh.read(4), 'big') # The byte order is big-endian
        height = int.from_bytes(fh.read(4), 'big')
        return (width, height)
