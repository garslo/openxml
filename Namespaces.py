#  Namespaces.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


from lxml import etree as et
from lxml.builder import ElementMaker

NS_A = 'http://schemas.openxmlformats.org/drawingml/2006/main'
NS_A14 = 'http://schemas.microsoft.com/office/drawing/2010/main'
NS_WPC = 'http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas'
NS_MC = 'http://schemas.openxmlformats.org/markup-compatibility/2006'
NS_O = 'urn:schemas-microsoft-com:office:office'
NS_R = 'http://schemas.openxmlformats.org/officeDocument/2006/relationships'
NS_M = 'http://schemas.openxmlformats.org/officeDocument/2006/math'
NS_V = 'urn:schemas-microsoft-com:vml'
NS_WP14 = 'http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing'
NS_WP = 'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing'
NS_W10 = 'urn:schemas-microsoft-com:office:word'
NS_W = 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'
NS_W14 = 'http://schemas.microsoft.com/office/word/2010/wordml'
NS_WPG = 'http://schemas.microsoft.com/office/word/2010/wordprocessingGroup'
NS_WPI = 'http://schemas.microsoft.com/office/word/2010/wordprocessingInk'
NS_WNE = 'http://schemas.microsoft.com/office/word/2006/wordml'
NS_WPS = 'http://schemas.microsoft.com/office/word/2010/wordprocessingShape'
NS_PIC = 'http://schemas.openxmlformats.org/drawingml/2006/picture'
NS_RELATIONSHIP = 'http://schemas.openxmlformats.org/package/2006/relationships'
NS_IMAGE = 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/image'


W = ElementMaker(namespace=NS_W, nsmap={'w': NS_W, 'r': NS_RELATIONSHIP})
WP = ElementMaker(namespace=NS_WP, nsmap={'wp': NS_WP})
A = ElementMaker(namespace=NS_A, nsmap={'a': NS_A})
A14 = ElementMaker(namespace=NS_A14, nsmap={'a14': NS_A14})
PIC = ElementMaker(namespace=NS_PIC, nsmap={'pic': NS_PIC})


EXT_URI = '{28A0092B-C50C-407E-A947-70E740481C1C}'


nsmap = {
    'a': NS_A,
    'wpc': NS_WPC,
    'mc': NS_MC,
    'o': NS_O,
    'r': NS_R,
    'm': NS_M,
    'v': NS_V,
    'w14': NS_WP14,
    'wp': NS_WP,
    'w10': NS_W10,
    'w': NS_W,
    'w14': NS_W14,
    'wpg': NS_WPG,
    'wpi': NS_WPI,
    'wne': NS_WNE,
    'wps': NS_WPS,
    'pic': NS_PIC,
    'relationship': NS_RELATIONSHIP,
    }


relmap = {
    'image': NS_IMAGE,
    }


def strip_ns_decls(xml):
    '''
    Removes all namespace declarations from `xml`.

    Parameters:
        - xml: A string containing xml

    Returns:
        A string void of any xml namespace declarations.
    '''
    return re.sub(b'\s*xmlns:\S+="\S+"', b'', xml)


def QN(ns, tag):
    '''
    A convenience function; runs lxml.etree.QName() on `tag` with
    namespace prefix `ns`.
    '''
    return et.QName(nsmap[ns], tag)


def N(ns, tag):
    return QN(ns, tag).text
