#  Managers.py
#
#  Copyright 2013 Gary Slopsema
#
#  This file is part of OpenXML for Python.
#
#  OpenXML for Python is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  OpenXML for Python is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with OpenXML for Python. If not, see
#  <http://www.gnu.org/licenses/>.


import os
import shutil

from lxml import etree as et
from lxml.builder import E

from OpenXML.Namespaces import nsmap
from OpenXML.core import *
from OpenXML.Media import Image


# TODO: Documentation for _everything_


_write_opts = {
    'encoding': 'UTF-8',
    'xml_declaration': True,
    }


class ContentTypeManager:
    '''
    `ContentTypeManager` handles adding new ContentTypes to the
    document (mainly everything in [Content_Types].xml).
    '''
    def __init__(self, docx):
        self._docx = docx
        self._content_type_etree = docx.etree('[Content_Types].xml')


    def add_default(self, extension, content_type):
        '''
        Adds a Default element to [Content_Types].xml. If `extension`
        is already present as a default, does nothing.

        Parameters:
            - extension: A file extension
            - content_type: The content type of files with extension `extension`

        Returns:
            None
        '''
        defaults = [elem.attrib['Extension'] for elem in list(self._content_type_etree)
                    if et.QName(elem).localname == 'Default']
        if extension not in defaults:
            new_default = E.Default(Extension=extension,
                                    ContentType=content_type)
            self._content_type_etree.append(new_default)


    def add_override(self, part_name, content_type):
        '''
        Adds an Override element to [Content_Types].xml. If
        `part_name` is already present as a default, does nothing.

        Parameters:
            - part_name: A Word document part name
            - content_type: The content type of `part_name`

        Returns:
            None
        '''
        overrides = [elem.attrib['PartName'] for elem in list(self._content_type_etree)
                     if et.QName(elem).localname == 'Override']
        if part_name not in overrides:
            new_override = E.Override(PartName=part_name,
                                      ContentType=content_type)
            self._content_type_etree.append(new_override)


    # TODO: Remove
    def write(self, base_path):
        self._content_type_etree(base_path + '[Content_Types].xml', **_write_opts)


class MediaManager:
    '''
    `MediaManager` handles adding and removing media to and from the
    document.
    '''
    def __init__(self, docx):
        self._docx = docx
        self._doc_etree = docx.etree('word/document.xml')
        self._media = []


    @property
    def img_rel_T(self):
        '''
        The image relationship type.
        '''
        return 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/image'


    # TODO: Cache the max_id? Just inc.
    def new_id(self, type_):
        '''
        Generates a new id of type `type_` (either 'drawing' or 'picture').

        Parameters:
            - type_: The id type ('drawing' or 'picture')

        Returns:
            An int that is a new valid id of type `type_`.
        '''
        nodes = []
        if type_ == 'drawing':
            nodes = self._doc_etree.findall('.//wp:docPr', namespaces=nsmap)
        elif type_ == 'picture':
            nodes = self._doc_etree.findall('.//pic:cNvPr', namespaces=nsmap)
        else:
            raise OpenXMLException('Cannot find id of type ' + str(type_) + '.')

        max_id = 0
        for node in nodes:
            id_ = int(node.attrib['id'])
            max_id = max(max_id, id_)
        return max_id+1


    def add_image(self, filename, node, description, type_='png', dpi=300):
        '''
        Adds the image stored in `filename` to the document, replacing `node`.

        Parameters:
            - filename: The name of the image file
            - node: The node to replace
            - description: The image's alt-text
            - type_: The image type. Defaults to 'png' (there are currently no other
                     options)
            - dpi: The dots-per-inch of the picture
        '''
        if type_ != 'png':
            raise OpenXMLException('Cannot handle image types other than png just yet.')

        base_name = os.path.basename(filename)
        # Get the ids
        rel_id = self._docx.relationship_manager.add_relationship(self.img_rel_T,
                                                                  'media/'+base_name)
        pic_id = self.new_id('picture')
        drawing_id = self.new_id('drawing')
        img = Image(filename, self._docx, node, description,
                    pic_id, drawing_id, rel_id, dpi=dpi)

        # Add it to the parent
        parent = node.getparent()
        new_img_node = et.XML(img.xml())
        parent.replace(node, new_img_node)

        # Store it for writing
        self._media.append(img)

        # Ensure we can handle this content type
        self._docx.content_type_manager.add_default('png', 'image/png')
        return img


    def write(self, base_path):
        for img in self._media:
            new_m = os.path.join(base_path, 'word/media/'+img.base_name)
            shutil.copy(img.name, new_m)


class RelationshipManager:
    def __init__(self, docx):
        self._docx = docx
        self._docx_rel_etree = docx.etree('_rels/.rels')
        self._document_rel_etree = docx.etree('word/_rels/document.xml.rels')


    # TODO: Cache the last, just inc for new?
    def new_id(self, type_='document'):
        '''
        Generates a new valid rId.

        Parameters:
            - type_: The id type

        Returns:
            A string containing a valid new relationship id.
        '''
        root = self._select_root(type_)
        max_id = 0
        for rel in list(root):
            # Each id is like 'rId###'
            id_ = int(rel.attrib['Id'][3:])
            max_id = max(max_id, id_)
        return 'rId'+str(max_id+1)


    def add_relationship(self, type_, target, to='document'):
        '''
        Updates `to' to include a relationship of type `type_' with
        target `target'. Returns the new id.

        `to' can be either 'docx' (for the whole archive) or
        'document' (for the word document). If you don't know the
        difference, you want 'document'.
        '''
        root = self._select_root(to)
        id_ = self.new_id(to)
        new_rel = E.Relationship(Id=id_,
                                 Type=type_,
                                 Target=target)
        root.append(new_rel)
        return id_


    def write(self, base_path, which='document'):
        '''
        Write the relationships of type `which' to `base_path'.
        '''
        if base_path[-1] != '/':
            base_path.append('/')
        if type_ == 'document':
            self._document_rel_etree.write(base_path + 'word/document.xml')
        elif type_ == 'docx':
            self._document_rel_etree.write(base_path + '_rel/.rels', **_write_opts)
        else:
            raise OpenXMLException('Cannot write relationships of type ' + str(which) + '.')


    def _select_root(self, type_):
        if type_ == 'document':
            return self._document_rel_etree
        elif type_ == 'docx':
            return self._docx_rel_etree
        raise OpenXMLException('Cannot find relationship type ' + str(type_) + '.')
